#include <iostream>

using namespace std;

int solution(int N)
{
    bool gapFlag = false;
    int gapCounter = 0;
    uint32_t indexBit = 0x0;
    int retMax = 0;

    if (N <= 0 || N > 2147483647) return 0;

    uint32_t N_u32 = static_cast<uint32_t>(N);
    while ((uint32_t)(0x1 << indexBit) < N_u32)
    {
        if ( ((0x1 << indexBit) & N_u32 ) != 0)
        {
            if (gapFlag == false)
            {
                gapFlag = true;
                gapCounter = 0;
            }
            else
            {
                if (retMax < gapCounter)
                {
                    retMax = gapCounter;
                }
                
                gapCounter= 0;
            }
            
        }
        else
        {
            if (gapFlag == true)
            {
                gapCounter = gapCounter + 1;
            }
            
        }
        indexBit++;
    }
    
    return retMax;
}

int main()
{
    int N = 1376796946;
    
    cout << "Result: " << solution(N) << endl;

    return 0;
}